/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2023. MIT licensed.
 */$(document).ready(function () {
  //show menu
  $('.navbar-toggle').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse,.modal-navbar').toggleClass('show');
  });
  $('.navbar-collapse .close,.modal-navbar').on('click', function () {
    $('.navbar-collapse,.modal-navbar').removeClass('show');
    $('.navbar-toggle').removeClass('active');
  });
  // active navbar of page current
  var urlcurrent = window.location.pathname;
  $(".navbar-nav li a[href$='" + urlcurrent + "']").addClass('active');

  // effect navbar
  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
      $('header').addClass('scroll');
    } else {
      $('header').removeClass('scroll');
    }
  });
  $('.fplus-link h4').on('click', function () {
    $(this).parent().toggleClass('show');
  });
  $(window).on("load", function (e) {
    $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
  });
  $('.navbar-nav > li').click(function () {
    $(this).toggleClass('active');
  });
  $('.btn_search').on('click', function () {
    $('.form-search input').focus();
    $('.form-search').toggleClass('show');
  });
});